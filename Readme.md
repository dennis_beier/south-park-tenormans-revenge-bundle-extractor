This a Open Source GPL v3 bundle extractor for the South Park Game  Tenorman's Revenge.

Start the programm with a bundle file as first parameter and then you get a list of the packed files.
Choose a number and the filename of the file you want to unpack.
If you want to improve things, just open a pull request.
Tested under Linux, but this is ISO C++ so it will work under windows as well.
Praised be St. iGNUtius.

build with cmake like this:

cmake .

make
