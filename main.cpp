#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <string>

using namespace std;

int main(int argc, char **argv)
{
    char* filename;
    if(argc == 2){
        filename = argv[1];
    }else {
        printf("no file name\n");
        return 1;
    }
    uint32_t bytes[2];
    FILE* file = fopen(filename, "r");
    if(file == nullptr){
        printf("Could not open file.\n");
        return 1;
    }
    fread(bytes,1,8,file);
    uint8_t *header = (uint8_t *) malloc(sizeof (uint8_t)*bytes[0]);
    printf("%d\n",bytes[0]);
    fread(header,1,bytes[0],file);
    int count = 1;
    std::string name = "";
    std::vector<std::string> names;
    std::vector<std::pair<uint,uint>> limits;
    for(int i = 0; i < bytes[0]; i++){
        if(header[i] == 0){
            uint32_t* p =  (uint32_t*) &(header[i+1]);
            uint32_t a1 = *p;
            uint32_t a2 = *(p+1);
            std::cout<<count <<" "<<name<<" " << a1 <<" "<< a2 <<endl;
            std::pair<uint,uint> limit = {bytes[0]+a1+8,a2};
            limits.push_back(limit);
            names.push_back(name);
            name = "";
            i+=8;
            count++;
        }
        name.append((char*)&header[i],1);
    }
    printf("%d\n",count);
    uint filen = 0;
    std::string fname;
    std::cout<<"Please enter file number and name"<<endl;
    std::cin>>filen>>fname;
    uint offset = limits[filen-1].first;
    uint length = limits[filen-1].second;
    char *c =(char*) malloc(length);
    fseek(file,offset,SEEK_SET);
    fread(c,1,length,file);
    FILE *out = fopen(fname.c_str(),"w+");
    fwrite(c, 1,length,out);
    fclose(out);
}
